=== Plugin Name ===
Contributors: bpip
Tags: help, documentation, help docs, docs, support docs, help portal
Requires at least: 3.0.1
Tested up to: 4.6.1
Stable tag: 4.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The quickest, easiest, and best help documentation plugin for WordPress.

== Description ==

The quickest, easiest, and best help documentation plugin for WordPress.

Creating help or support documentation is a vital part of any online business. Whether you are selling software, products, memberships, or just run a blog, creating easy to use help documentation is very important. I created this plugin to make it dead simple to create your own help portal where your users can easily find the answers to the questions they may have.

We also use ajax to paginate through more help docs per category cutting down on page load time if you have lots of help docs. Checkout the screenshots to get more information.

== Installation ==

1. Upload 'wp-help-docs' folder to the '/wp-content/plugins/' directory. Or install the zip file to Plugins > Add New
2. Activate the plugin through the 'Plugins' menu in WordPress.

== Frequently Asked Questions ==

= Is WP Help Docs Free? =

Yep! Use it as much as you like and on as many sites as you like.

= How do I set it up? =

* First make sure the plugin is installed and activated. 
* Then you'll see a link in the left hand column titled "Help Docs", navigate there and start adding new help docs. 
* You should also create help doc categories for organization. 
* For best results, you should only assign one category per help doc.
* Create a page and add the shortcode: '[help-docs]' inside the content area.
* Optionally also add the shortcode: '[help-docs-sidebar]' to a sidebar text widget. This lists similar help docs (by help doc category) in the sidebar when viewing a single help doc post. Please note that your theme must have a sidebar widget area for this to work correctly.

== Screenshots ==

1. screenshot-1.png
2. screenshot-2.png
3. screenshot-3.png
4. screenshot-4.png
5. screenshot-5.png

== Changelog ==

= 1.0.0 =
* Initial release

== Upgrade Notice ==