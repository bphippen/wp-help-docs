<?php
/**
 * Plugin Name:       WP Help Docs
 * Plugin URI:        https://wphelpdocs.co
 * Description:       The quickest, easiest, and best help documentation plugin for WordPress.
 * Version:           1.0.0
 * Author:            BPIP
 * Author URI:        https://wphelpdocs.co
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wphd
 * Domain Path:       /languages
 */


// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;


/**
 * Main WP_Help_Docs Class.
 *
 * @since 1.0.0
 */
final class WP_Help_Docs {
	/** Singleton **/

	/**
	 * @var WP_Help_Docs The one true WP_Help_Docs
	 * @since 1.0.0
	 */
	private static $instance;

	/**
	 * WPHD Settings Object.
	 *
	 * @var object|WPHD_Settings
	 * @since 1.5
	 */
	public $settings;

	/**
	 * Main WP_Help_Docs Instance.
	 *
	 * Insures that only one instance of WP_Help_Docs exists in memory at any one
	 * time. Also prevents needing to define globals all over the place.
	 *
	 * @since 1.0.0
	 * @static
	 * @uses WP_Help_Docs::setup_constants() Setup the constants needed.
	 * @uses WP_Help_Docs::includes() Include the required files.
	 * @see WPHD()
	 * @return object|WP_Help_Docs The one true WP_Help_Docs
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof WP_Help_Docs ) ) {
			self::$instance = new WP_Help_Docs;
			self::$instance->setup_constants();
			self::$instance->includes();
			self::$instance->settings = new WPHD_Settings();
		}
		return self::$instance;
	}


	/**
	 * Throw error on object clone.
	 *
	 * The whole idea of the singleton design pattern is that there is a single
	 * object therefore, we don't want the object to be cloned.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @return void
	 */
	public function __clone() {
		// Cloning instances of the class is forbidden.
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wphd' ), '1.0.0' );
	}


	/**
	 * Disable unserializing of the class.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @return void
	 */
	public function __wakeup() {
		// Unserializing instances of the class is forbidden.
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wphd' ), '1.0.0' );
	}


	/**
	 * Setup plugin constants.
	 *
	 * @access private
	 * @since 1.0.0
	 * @return void
	 */
	private function setup_constants() {
		// Plugin version.
		if ( ! defined( 'WPHD_VERSION' ) ) {
			define( 'WPHD_VERSION', '1.0.0' );
		}
		// Plugin Folder Path.
		if ( ! defined( 'WPHD_PLUGIN_PATH' ) ) {
			define( 'WPHD_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
		}
		// Plugin Folder URL.
		if ( ! defined( 'WPHD_PLUGIN_URL' ) ) {
			define( 'WPHD_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		}
		// Plugin Root File.
		if ( ! defined( 'WPHD_PLUGIN_FILE' ) ) {
			define( 'WPHD_PLUGIN_FILE', __FILE__ );
		}
		// Development env
		if ( ! defined( 'WPHD_DEV_ENV' ) ) {
			define( 'WPHD_DEV_ENV', 'http://wphdplugin.dev' );
		}
		// The URL our updater / license checker pings
		//define( 'WPHD_URL', 'https://www.wphelpdocs.co' );
		// The name of your plugin
		//define( 'WPHD_NAME', 'WP Help Docs' );
		// The name of the settings page
		//define( 'WPHD_LICENSE_PAGE', 'wphd-license' );
	}


	/**
	 * Include required files.
	 *
	 * @access private
	 * @since 1.0.0
	 * @return void
	 */
	private function includes() {
		require_once WPHD_PLUGIN_PATH . 'inc/admin/class-settings-api.php';
		require_once WPHD_PLUGIN_PATH . 'inc/admin/class-settings.php';
		require_once WPHD_PLUGIN_PATH . 'inc/admin/post-types.php';
		require_once WPHD_PLUGIN_PATH . 'inc/enqueue.php';
		require_once WPHD_PLUGIN_PATH . 'inc/ajax.php';
		require_once WPHD_PLUGIN_PATH . 'inc/shortcodes.php';
		require_once WPHD_PLUGIN_PATH . 'inc/template-func.php';
		require_once WPHD_PLUGIN_PATH . 'inc/frontend-func.php';
	}

}


/**
 * The main function that returns WP Help Docs
 *
 * @since 1.0.0
* @return object|WP_Help_Docs The one true WP_Help_Docs Instance.
 */
function WPHD() {
	return WP_Help_Docs::instance();
}


// Get WPHD Running.
WPHD();