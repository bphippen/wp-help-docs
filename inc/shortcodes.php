<?php
/**
 * Shortcode Functions
 *
 * @package     WPHD
 * @subpackage  Functions
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


// allow shortcodes in widgets
add_filter('widget_text', 'do_shortcode');


// add help docs shortcode
add_shortcode( 'help-docs', 'wphd_help_docs_shortcode' );
function wphd_help_docs_shortcode() {
	ob_start();
	wphd_get_template_part('help-main');
	$help_docs_index = ob_get_clean();
	return $help_docs_index;
}


// add help docs sidebar shortcode
add_shortcode( 'help-docs-sidebar', 'wphd_help_docs_sidebar_shortcode' );
function wphd_help_docs_sidebar_shortcode() {
	ob_start();
	wphd_get_template_part('help-sidebar');
	$help_docs_sidebar = ob_get_clean();
	return $help_docs_sidebar;
}