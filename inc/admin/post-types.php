<?php
/**
 * Custom Post Type Functions
 *
 * @package     WPHD
 * @subpackage  Functions
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// register the post types / taxonomies on plugin activation and flush rewrite rules
register_activation_hook( WPHD_PLUGIN_FILE, 'wphd_plugin_activate' );
function wphd_plugin_activate() {
    wphd_documentation_post_type();
    wphd_documentation_category_taxonomy();
    flush_rewrite_rules();
}


// Register the documentation custom post type.
add_action( 'init', 'wphd_documentation_post_type' );
function wphd_documentation_post_type() {
	$labels = array(
		'name'                  => _x( 'Documents', 'Post Type General Name', 'wp-help-docs' ),
		'singular_name'         => _x( 'Document', 'Post Type Singular Name', 'wp-help-docs' ),
		'menu_name'             => __( 'Help Docs', 'wp-help-docs' ),
		'name_admin_bar'        => __( 'Document', 'wp-help-docs' ),
		'archives'              => __( 'Document Archives', 'wp-help-docs' ),
		'parent_item_colon'     => __( 'Parent Document:', 'wp-help-docs' ),
		'all_items'             => __( 'All Documents', 'wp-help-docs' ),
		'add_new_item'          => __( 'Add New Document', 'wp-help-docs' ),
		'add_new'               => __( 'Add New', 'wp-help-docs' ),
		'new_item'              => __( 'New Document', 'wp-help-docs' ),
		'edit_item'             => __( 'Edit Document', 'wp-help-docs' ),
		'update_item'           => __( 'Update Document', 'wp-help-docs' ),
		'view_item'             => __( 'View Document', 'wp-help-docs' ),
		'search_items'          => __( 'Search Document', 'wp-help-docs' ),
		'not_found'             => __( 'Not found', 'wp-help-docs' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wp-help-docs' ),
		'featured_image'        => __( 'Featured Image', 'wp-help-docs' ),
		'set_featured_image'    => __( 'Set featured image', 'wp-help-docs' ),
		'remove_featured_image' => __( 'Remove featured image', 'wp-help-docs' ),
		'use_featured_image'    => __( 'Use as featured image', 'wp-help-docs' ),
		'insert_into_item'      => __( 'Insert into Document', 'wp-help-docs' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Document', 'wp-help-docs' ),
		'items_list'            => __( 'Documents list', 'wp-help-docs' ),
		'items_list_navigation' => __( 'Documents list navigation', 'wp-help-docs' ),
		'filter_items_list'     => __( 'Filter Documents list', 'wp-help-docs' ),
	);
	$args = array(
		'label'                 => __( 'Document', 'wp-help-docs' ),
		'description'           => __( 'Documents', 'wp-help-docs' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-info',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite'				=> array( 'slug' => 'doc', 'with_front' => false ),
	);
	register_post_type( 'wphd_doc', $args );
}


// Register the documentation category taxonomy.
add_action( 'init', 'wphd_documentation_category_taxonomy' );
function wphd_documentation_category_taxonomy() {
	$labels = array(
		'name'                       => _x( 'Document Categories', 'Taxonomy General Name', 'wp-help-docs' ),
		'singular_name'              => _x( 'Document Category', 'Taxonomy Singular Name', 'wp-help-docs' ),
		'menu_name'                  => __( 'Doc Categories', 'wp-help-docs' ),
		'all_items'                  => __( 'All Document Categories', 'wp-help-docs' ),
		'parent_item'                => __( 'Parent Document Category', 'wp-help-docs' ),
		'parent_item_colon'          => __( 'Parent Document Category:', 'wp-help-docs' ),
		'new_item_name'              => __( 'New Document Category Name', 'wp-help-docs' ),
		'add_new_item'               => __( 'Add New Document Category', 'wp-help-docs' ),
		'edit_item'                  => __( 'Edit Document Category', 'wp-help-docs' ),
		'update_item'                => __( 'Update Document Category', 'wp-help-docs' ),
		'view_item'                  => __( 'View Document Category', 'wp-help-docs' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'wp-help-docs' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'wp-help-docs' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wp-help-docs' ),
		'popular_items'              => NULL,
		'search_items'               => __( 'Search Items', 'wp-help-docs' ),
		'not_found'                  => __( 'Not Found', 'wp-help-docs' ),
		'no_terms'                   => __( 'No items', 'wp-help-docs' ),
		'items_list'                 => __( 'Items list', 'wp-help-docs' ),
		'items_list_navigation'      => __( 'Items list navigation', 'wp-help-docs' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'rewrite'           		 => array( 'slug' => 'docs-category' ),
	);
	register_taxonomy( 'wphd_cat', array( 'wphd_doc' ), $args );
}