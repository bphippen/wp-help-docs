<?php
if ( !class_exists('Wphd_Settings' ) ):
class WPHD_Settings {


	private $settings_api;


	function __construct() {
		$this->settings_api = new WeDevs_Settings_API;
		add_action( 'admin_init', array($this, 'admin_init') );
		add_action( 'admin_menu', array($this, 'admin_menu') );
	}


	function admin_init() {
		//set the settings
		$this->settings_api->set_sections( $this->get_settings_sections() );
		$this->settings_api->set_fields( $this->get_settings_fields() );
		//initialize settings
		$this->settings_api->admin_init();
	}


	function admin_menu() {
		add_submenu_page( 'edit.php?post_type=wphd_doc', 'WP Help Docs Settings', 'Settings', 'delete_posts', 'wp_help_docs_settings', array($this, 'plugin_page') );
	}


	function get_settings_sections() {
		$sections = array(
			array(
				'id'    => 'wphd_general',
				'title' => __( 'General Settings', 'wp-help-docs' )
			),
			array(
				'id'    => 'wphd_help',
				'title' => __( 'Help', 'wp-help-docs' )
			)
		);
		return $sections;
	}


	// Returns all the settings fields
	function get_settings_fields() {
		$settings_fields = array(
			'wphd_general' => array(
				array(
					'name'              => 'wphd_index',
					'label'             => __( 'Main help docs page', 'wp-help-docs' ),
					'desc'              => __( 'What is the URL to the page where the shortcode lives?', 'wp-help-docs' ),
					'placeholder'       => __( '/docs/', 'wp-help-docs' ),
					'type'              => 'text',
					'default'           => '/docs/',
					'sanitize_callback' => 'sanitize_text_field'
				),
				array(
					'name'              => 'wphd_back_to_index',
					'label'             => __( 'Back to help docs text', 'wp-help-docs' ),
					'desc'              => __( 'The text to display for going back to the help docs index page.', 'wp-help-docs' ),
					'placeholder'       => __( 'Back To Documentation', 'wp-help-docs' ),
					'type'              => 'text',
					'default'           => 'Back To Documentation',
					'sanitize_callback' => 'sanitize_text_field'
				),
				array(
					'name'              => 'wphd_articles_per_post',
					'label'             => __( 'Max # of articles', 'wp-help-docs' ),
					'desc'              => __( 'How many articles would you like to show per category per page? This determines when to paginate.', 'wp-help-docs' ),
					'placeholder'       => __( '10', 'wp-help-docs' ),
					'min'               => 1,
					'max'               => 50,
					'step'              => '1',
					'type'              => 'number',
					'default'           => '10',
					'sanitize_callback' => 'floatval'
				),
				array(
					'name'              => 'wphd_column_number',
					'label'             => __( '# of columns', 'wp-help-docs' ),
					'desc'              => __( 'How many columns do you want to span across the main help docs page?', 'wp-help-docs' ),
					'type'              => 'select',
					'default'           => '3',
					'options' => array(
                        '1' => '1',
                        '2' => '2',
                        '3' => '3'
                    )
				),
			),
			'wphd_help' => array(
				array(
					'name'        => 'wphd_help_shortcodes',
					'label'   => __( 'Shortcodes', 'wp-help-docs' ),
					'desc'        => __( 'Add the shortcode: <strong>[help-docs]</strong> to the page you want to be your main help docs page.<br>Add the shortcode: <strong>[help-docs-sidebar]</strong> to a sidebar widget area to show a list of help docs on a single help doc post. Please note, this will only show on a help doc post.', 'wp-help-docs' ),
					'type'        => 'html'
				),
				array(
					'name'        => 'wphd_help_link',
					'label'   => __( 'Our help docs', 'wp-help-docs' ),
					'desc'        => __( 'Thanks so much for installing <strong>WP Help Docs</strong>. If you would like to read our own help docs, you can read them here: <br><a href="https://www.wphelpdocs.co/docs/" target="_blank">https://www.wphelpdocs.co/docs/</a>', 'wp-help-docs' ),
					'type'        => 'html'
				),
				array(
					'name'        => 'wphd_other_plugins',
					'label'   => __( 'Our other plugins', 'wp-help-docs' ),
					'desc'        => __( 'Be sure to check out some of our other awesome plugins.<br><a href="https://www.wpultimatereviews.com/" target="_blank">WP Ultimate Reviews</a><br><a href="https://www.wpultimateguides.com/" target="_blank">WP Ultimate Guides</a><br><a href="https://www.wphelpdocs.co/" target="_blank">WP Help Docs</a>', 'wp-help-docs' ),
					'type'        => 'html'
				),
			),
		);
		return $settings_fields;
	}


	function plugin_page() {
		echo '<div class="wrap">';
		$this->settings_api->show_navigation();
		$this->settings_api->show_forms();
		echo '</div>';
	}


	// Get all the pages
	function get_pages() {
		$pages = get_pages();
		$pages_options = array();
		if ( $pages ) {
			foreach ($pages as $page) {
				$pages_options[$page->ID] = $page->post_title;
			}
		}
		return $pages_options;
	}


}
endif;