<?php
/**
 * Ajax Functions
 *
 * @package     WPHD
 * @subpackage  Functions
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


// Ajax pagination function
add_action( 'wp_ajax_nopriv_ajax_pagination', 'wphd_ajax_pagination' );
add_action( 'wp_ajax_ajax_pagination', 'wphd_ajax_pagination' );
function wphd_ajax_pagination() {
	$paged = $_POST['page'];
	$cat = $_POST['cat'];

	$args = array(
		'post_type' => 'wphd_doc',
		'posts_per_page' => wphd_get_option('wphd_articles_per_post', 'wphd_general'),
		'wphd_cat' => $cat,
		'paged' => $paged,
		'orderby' => apply_filters('wphd_post_orderby', 'date'),
		'order' => apply_filters('wphd_post_order', 'ASC'),
	);

	$articles = new WP_Query( $args );

	if ( $articles->have_posts() ) : while ( $articles->have_posts() ) : $articles->the_post(); { ?>
		<li class="wphd-article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	<?php }
	wp_reset_postdata();
	endwhile; endif;
	die();
}