<?php
/**
 * Enqueue Functions
 *
 * @package     WPHD
 * @subpackage  Functions
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


// enqueue scripts / styles
add_action( 'wp_enqueue_scripts', 'wphd_enqueue_scripts_styles' );
function wphd_enqueue_scripts_styles() {
	$js_dir = WPHD_PLUGIN_URL . 'assets/js/';
	$css_dir = WPHD_PLUGIN_URL . 'assets/css/';
	// custom css
	wp_enqueue_style( 'wphd-style', $css_dir . 'style.css', array(), WPHD_VERSION, 'all' );
	// custom script
	wp_enqueue_script( 'wphd-script-js', $js_dir . 'scripts.js', array('jquery'), WPHD_VERSION, true );
	wp_localize_script( 'wphd-script-js', 'ajaxpagination', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' )
		));
}