<?php
/**
 * Front End Functions
 *
 * @package     WPHD
 * @subpackage  Functions
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


// Get plugin option values
function wphd_get_option( $option, $section, $default = '' ) {
    $options = get_option( $section );
    if ( isset( $options[$option] ) ) {
        return $options[$option];
    }
    return $default;
}


// active class for sidebar articles
function wphd_maybe_add_active_class($article) {
	global $post;
	if ($post->ID === $article->ID) {
		echo 'active';
	} else {
		echo '';
	}
}


// Filter archive the_content. Used on archives of help cat
add_filter( 'the_content', 'wphd_filter_archive_content' );
function wphd_filter_archive_content($content) {
	if (!is_admin() && is_archive('wphd_cat')) {
		$excerpt_length = apply_filters( 'excerpt_length', 55 );
		$excerpt_more = apply_filters( 'excerpt_more', ' ' . '[&hellip;]' );
		$content = wp_trim_words( $content, $excerpt_length, $excerpt_more );
	}
	return $content;
}