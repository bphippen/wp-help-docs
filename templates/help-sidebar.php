<?php  
	global $post;
	// bail if not on a help doc post
	if (!is_singular('wphd_doc')) {
		return;
	}
	$help_cats = wp_get_post_terms( $post->ID, 'wphd_cat' );
	$first_help_cat_slug = (!empty($help_cats)) ? $help_cats[0]->slug : '';
	$first_help_cat_name = (!empty($help_cats)) ? $help_cats[0]->name : __('All Help Docs', 'wphd-help-docs');
	$articles = get_posts(array(
		'post_type' => 'wphd_doc',
		'posts_per_page' => apply_filters('wphd_posts_per_page', '50'),
		'wphd_cat' => $first_help_cat_slug,
		'orderby' => apply_filters('wphd_post_orderby', 'date'),
		'order' => apply_filters('wphd_post_order', 'ASC'),
	));
?>

<div class="wphd-sidebar">
	<h3><?php echo $first_help_cat_name; ?></h3>
	<ul class="wphd-article-list-sidebar">
		<?php foreach ($articles as $article) : ?>
			<li class="wphd-article-title <?php wphd_maybe_add_active_class($article); ?>"><a href="<?php echo get_permalink($article); ?>"><?php echo get_the_title($article); ?></a></li>
		<?php endforeach; ?>
	</ul>
	<div class="wphd-back">
		<div class="wphd-hr"></div>
		<p><a href="<?php echo wphd_get_option('wphd_index', 'wphd_general'); ?>"><?php echo wphd_get_option('wphd_back_to_index', 'wphd_general'); ?></a></p>
	</div>
</div>