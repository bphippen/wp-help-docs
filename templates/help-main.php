<?php 
	$columns = wphd_get_option('wphd_column_number', 'wphd_general');
	$categories = get_terms( 'wphd_cat', array(
		'hide_empty' => true,
		'parent' => '0',
		'orderby' => apply_filters('wphd_cat_orderby', 'order'), // use wp term order plugin to drag and drop cats
		'order' => apply_filters('wphd_cat_order', 'ASC'),
	));
 ?>

 <?php if ($categories) : ?>
	<div class="wphd-category-index">
		<div class="wphd-grid">
			<?php $i = 0; foreach ($categories as $cat) : $i++; ?>
				<div class="wphd-1-<?php echo $columns; ?> wphd-column">
					<div class="wphd-cat-box">
						<div class="wphd-cat-name"><h3><?php echo $cat->name; ?></h3></div>
						<?php if ($cat->description) : ?>
							<div class="wphd-cat-description"><?php echo $cat->description; ?></div>
						<?php endif; ?>
						<a href="<?php echo get_term_link($cat); ?>" class="wphd-cat-count"><?php echo sprintf( _n( '%s Article', '%s Articles', $cat->count, 'wp-help-docs' ), $cat->count ); ?></a>
						<div class="wphd-articles-list wphd-list-<?php echo $i; ?>" style="display: none;" data-cat="<?php echo $cat->slug; ?>">
							<?php
								$a = 'paged'.$i;
								$b = isset( $_GET[$a] ) ? (int) $_GET[$a] : 1;
								$args = array(
									'post_type' => 'wphd_doc',
									'posts_per_page' => wphd_get_option('wphd_articles_per_post', 'wphd_general'),
									'wphd_cat' => $cat->slug,
									'paged' => $b,
									'orderby' => apply_filters('wphd_post_orderby', 'date'),
									'order' => apply_filters('wphd_post_order', 'ASC'),
								);
								$articles = new WP_Query( $args );
							?>
							<ul>
								<?php if ( $articles->have_posts() ) : while ( $articles->have_posts() ) : $articles->the_post(); ?>
									<li class="wphd-article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
								<?php wp_reset_postdata(); ?>
								<?php endwhile; endif; ?>
							</ul>
							<div class="wphd-pagination">
								<?php
									echo paginate_links( array(
										'format' => '?'.$a.'=%#%',
										'current' => $a,
										'show_all' => true,
										'total' => $articles->max_num_pages
									) );
								?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php else : ?>
	<?php echo __('No help docs found!', 'wp-help-docs'); ?>
<?php endif; ?>