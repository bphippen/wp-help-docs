(function( $ ) {
	'use strict';

	// expand doc category
	$('.wphd-cat-count').click(function(e) {
		e.preventDefault();
		$(this).next().slideToggle();
	});


	// ajax call for more help docs
	$('.wphd-articles-list a.page-numbers:first-child').addClass('active');
	$(document).on( 'click', '.wphd-articles-list a.page-numbers', function( e ) {
		e.preventDefault();

		var clicky = $(this);
		var page = parseInt( clicky.html() );
		var cat = clicky.closest('.wphd-articles-list').data('cat');
		var height = clicky.closest('.wphd-articles-list').find('ul').height();
		clicky.closest('.wphd-articles-list').find('ul').css('min-height', height);
		clicky.addClass('active');
		clicky.siblings().removeClass('active');
		
		$.ajax({
			async: true,
			url: ajaxpagination.ajaxurl,
			type: 'post',
			data: {
				action: 'ajax_pagination',
				cat: cat,
				page: page
			},
			beforeSend: function() {
				clicky.closest('.wphd-articles-list').find('ul li').remove();
				clicky.closest('.wphd-articles-list').find('ul').prepend('<li id="loader">Loading</li>');
			},
			success: function( html ) {
				clicky.closest('.wphd-articles-list').find('#loader').remove();
				clicky.closest('.wphd-articles-list').find('ul').append(html);
			}
		})
	});

})( jQuery );
